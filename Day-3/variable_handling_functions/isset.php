<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$var='';
//This will evaluate to TRUE so the text will be printed.
if (isset($var)){
    echo "This var is set so I will print.";
}

$a="test";
$b="anothertest";
var_dump(isset($a));//true
var_dump(isset($a,$b));//false
unset ($a);

var_dump(isset($a));//false
var_dump(isset($a,$b));//true

$foo=null;
var_dump(isset($foo));//false